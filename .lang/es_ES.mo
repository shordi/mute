��    -      �  =   �      �  (   �     
  
        !     1     7     >     D     X     a     n          �  
   �     �     �  :   �               0  "   9     \  _   k     �     �  !   �          (     G     S     X     m  H   z     �     �     �     �  2   �     (  	   5  	   ?     I     W     e  �   �  -   U     �     �     �     �     �     �     �  	   �     �     �     	  *   .	     Y	     f	     k	  L   	     �	     �	  	   �	  '   
     -
  l   <
     �
     �
  #   �
     �
           ,     9      ?     `  \   o  
   �     �     �     �  <        Q  	   j     t     �     �      �            -                  $      	   !   (   "   %   ,            
       &                               '                 +                               #         *                              )               Actual Settings are the Default Settings Add Console Background Blinking Cursor Block Cancel Close Close MultiTerminal Continue Cursor Style Default Settings Delete Actual Settings Font of Main Window Title Foreground Grid Hide the Ugly Guy Hide ugly Shordi's face and show nice Mute's Logo instead. Init Default Mode Maximize/Restore Minimize Move MultiTerminal to a New Window Multi Terminal Multi Terminal made with Gambas 3 who allows two different ways of the terminals visualization. Mute. By Shordi. Mute: Preferences Mute<br><br>Just a Multi Terminal New MultiTerminal below New MultiTerminal on the right Preferences Quit Save Actual Settings Save Changes Settings Saved. They will take effect on next program restart. Quit Now? Tab 1 Tabs Terminal's Font Terminal's Preferences This action sets the default settings for teminals Title's Font Underline Unpin Tab Version:      Vertical Line Visualization Mode at init Project-Id-Version: Mute 3.19.3
PO-Revision-Date: 2024-08-07 16:53 UTC
Last-Translator: jorge <jorge@abu>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 La Configuración actual ya es la por defecto Añadir Consola Fondo Cursor Parpadeante Bloque Cancelar Cerrar Cerrar MultiTerminal Continuar Estilo de Cursor Configuración por Defecto Borrar la Configuración Actual Fuente del Título de la Ventana Principal Primer Plano Grid Ocultar el Tío Feo Ocultar el feo careto de Shordi y mostrar el bonito logo de Mute en su lugar Modo por defecto al inicio Maximizar/Restaurar Minimizar Mover Multiterminal a una nueva ventana Multi Terminal Multi Terminal realizado con Gambas 3 que permite dos formas diferentes de visualización de los terminales. Mute.By Shordi. Mute: Preferencias Mute<br><br>Sólo un Multi Terminal Nuevo Multiterminal debajo Nuevo MultiTerminal a la derecha Preferencias Salir Guardar la Configuración Actual Salvar Cambios Configuración Guardada. Tendrá efecto en el próximo reinicio del programa. ¿Salir Ahora? Pestaña 1 Tabs Fuente de los terminales Preferencias de los Terminales Esta acción establece la configuración para los terminales Fuente para los títulos Subrayado Desanclar la Pestaña Versión:      Línea Vertical Modo de Visualización al inicio 